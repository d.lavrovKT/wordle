package com.lavrovdmitriii.wordle.uielements

import com.lavrovdmitriii.wordle.databinding.ActivityStartBinding

class LogoChars(binding: ActivityStartBinding) {
    var logoChars = arrayOf(
        binding.tvLogoChar1,
        binding.tvLogoChar2,
        binding.tvLogoChar3,
        binding.tvLogoChar4,
        binding.tvLogoChar5,
        binding.tvLogoChar6,
        binding.tvLogoChar7,
        binding.tvLogoChar8
    )
}