package com.lavrovdmitriii.wordle.uielements

import com.lavrovdmitriii.wordle.databinding.ActivityMainBinding

class MainScreenElements(binding: ActivityMainBinding) {
    var screenElements = arrayOf(
        binding.btnRules,
        binding.btnExitGame,
        binding.btnStart,
        binding.WinStats,
        binding.LostStats,
        binding.GamesStats,
        binding.PercentStats,
        binding.btnStats
    )
}