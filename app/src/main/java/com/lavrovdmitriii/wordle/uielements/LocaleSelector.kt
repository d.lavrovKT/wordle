package com.lavrovdmitriii.wordle.uielements

object LocaleSelector {
    var lang = "en"

    val ruLangArr = arrayListOf(
        "ru",
        "uk",
        "be",
        "hy",
        "kk"
    )
}