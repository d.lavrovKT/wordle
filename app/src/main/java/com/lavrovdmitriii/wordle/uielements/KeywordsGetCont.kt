package com.lavrovdmitriii.wordle.uielements

import android.widget.TextView

interface KeywordsGetCont {
    fun getKeywordsElementsId(keyworsdArr: Array<TextView>): MutableList<Int> {
        val resultArr = mutableListOf<Int>()
        for (element in keyworsdArr) {
            resultArr.add(element.id)
        }
        return resultArr
    }

    fun getKeywordsElementsText(keyworsdArr: Array<TextView>): MutableList<String> {
        val resultArr = mutableListOf<String>()
        for (element in keyworsdArr) {
            resultArr.add(element.text.toString())
        }
        return resultArr
    }
}