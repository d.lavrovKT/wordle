package com.lavrovdmitriii.wordle

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.GridLayoutManager
import com.lavrovdmitriii.wordle.constants.BACK_IS_STATS
import com.lavrovdmitriii.wordle.constants.UNIQUE_USER_ID
import com.lavrovdmitriii.wordle.constants.user
import com.lavrovdmitriii.wordle.data.ReadDatabase
import com.lavrovdmitriii.wordle.data.User
import com.lavrovdmitriii.wordle.data.users
import com.lavrovdmitriii.wordle.databinding.ActivityGlobalStatsBinding
import com.lavrovdmitriii.wordle.rwadapters.UsersListAdapter

class GlobalStats : AppCompatActivity() {
    private lateinit var binding: ActivityGlobalStatsBinding
    private var readDatabase = ReadDatabase()
    private lateinit var myPreferences: SharedPreferences
    private lateinit var usersListAdapter: UsersListAdapter
    private var scrollPosition = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGlobalStatsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        myPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val cutUsers = arrayListOf<User>()
        for (item in 0 until users.size) {
            if (item < 20) {
                cutUsers.add(users[item])
            }
        }
        usersListAdapter = UsersListAdapter(cutUsers, myPreferences)
        refreshUsersList()
        binding.usersList.layoutManager = GridLayoutManager(this, 1)
        getActivePlayerStats()

        if (intent.getStringExtra("key") != "playactivity") {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("key", BACK_IS_STATS)
            binding.backBtn.setOnClickListener {
                startActivity(intent)
            }
            binding.personalStatsBtn.setOnClickListener {
                startActivity(Intent(this, StatsActivity::class.java))
            }
        } else {
            binding.backBtn.setOnClickListener {
                finish()
            }
            binding.personalStatsBtn.setOnClickListener {
                val intent = Intent(this, StatsActivity::class.java)
                intent.putExtra("key", "playactivity")
                startActivity(intent)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        users = arrayListOf()
        readDatabase.setDatabaseCont()
    }

    private fun refreshUsersList() {
        readDatabase.setDatabaseCont()
        binding.usersList.adapter = usersListAdapter
    }

    private fun getActivePlayerStats() {
        for (item in 0 until users.size) {
            if (users[item].uniqueID == myPreferences.getString(UNIQUE_USER_ID, "default")) {
                binding.nicknameActiveUser.text = users[item].nickname
                binding.pointsActiveUser.text = users[item].points.toString()
                binding.activeUserPos.text = (item + 1).toString()
                binding.userFlag3.setCountryForNameCode(users[item].flagID)
            }
        }
    }
}