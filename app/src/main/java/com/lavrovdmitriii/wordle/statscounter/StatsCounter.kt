package com.lavrovdmitriii.wordle.statscounter

import android.content.SharedPreferences
import com.google.firebase.firestore.FirebaseFirestore
import com.lavrovdmitriii.wordle.constants.UNIQUE_USER_ID
import com.lavrovdmitriii.wordle.constants.USER_POINTS
import com.lavrovdmitriii.wordle.constants.userPoints
import com.lavrovdmitriii.wordle.data.ReadDatabase
import com.lavrovdmitriii.wordle.data.users
import com.yandex.mobile.ads.impl.it

class StatsCounter(var myPreferences: SharedPreferences, private val db: FirebaseFirestore) {

    private fun getUserPoints(): Int {
        val firstWinRes = myPreferences.getInt("WINWORD1", 0)
        val secondWinRes = myPreferences.getInt("WINWORD2", 0)
        val threeWinRes = myPreferences.getInt("WINWORD3", 0)
        val fourWinRes = myPreferences.getInt("WINWORD4", 0)
        val fiveWinRes = myPreferences.getInt("WINWORD5", 0)
        val sixWinRes = myPreferences.getInt("WINWORD6", 0)
        userPoints = sixWinRes + fiveWinRes * 2 + fourWinRes * 3 + threeWinRes * 5 + secondWinRes * 7 + firstWinRes * 10
        return userPoints
    }

    fun updateUserPointsPref() {
        myPreferences.edit().putInt(USER_POINTS, getUserPoints()).apply()
    }

    fun updateUserPointsDB() {
        db.collection("users")
            .document(myPreferences.getString(UNIQUE_USER_ID, "default").toString())
            .update("points", myPreferences.getInt(USER_POINTS, 0))
    }
}