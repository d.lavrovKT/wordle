package com.lavrovdmitriii.wordle

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.lavrovdmitriii.wordle.databinding.ActivityRulesBinding
import com.lavrovdmitriii.wordle.uielements.LocaleSelector

class RulesActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRulesBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRulesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (intent.getStringExtra("key") == "playactivity") {
            binding.btnToPlay.text = getString(R.string.back_to_game)
            binding.btnToPlay.setOnClickListener {
                finish()
            }
        }
    }

    fun startInRules(view: View) {
        val intent: Intent = if (LocaleSelector.ruLangArr.contains(LocaleSelector.lang)) {
            Intent(this@RulesActivity, PlayActivity::class.java)
        } else {
            Intent(this@RulesActivity, PlayEnActivity::class.java)
        }
        startActivity(intent)
    }
}