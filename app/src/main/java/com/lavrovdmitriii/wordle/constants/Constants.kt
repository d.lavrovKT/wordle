package com.lavrovdmitriii.wordle.constants

const val IS_REG_USER = "isRegUser"
const val USER_NAME = "userName"
const val COUNTRY_CODE_USER = "countryCodeUser"
const val UNIQUE_USER_ID = "uniqueUserId"
const val USER_POINTS = "userPoints"
const val BACK_IS_STATS = "backIsStats"
const val REQUIRED_SEND_WORD_COUNT = "countVisibleModalSend"