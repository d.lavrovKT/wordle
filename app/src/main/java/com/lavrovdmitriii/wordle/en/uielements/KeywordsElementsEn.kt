package com.lavrovdmitriii.wordle.en.uielements

import android.widget.TextView
import com.lavrovdmitriii.wordle.databinding.ActivityPlayEnBinding
import com.lavrovdmitriii.wordle.uielements.KeywordsGetCont

class KeywordsElementsEn(binding: ActivityPlayEnBinding) : KeywordsGetCont {
    var keywordsArr: Array<TextView> = arrayOf(
        binding.tvKey1,
        binding.tvKey3,
        binding.tvKey4,
        binding.tvKey5,
        binding.tvKey6,
        binding.tvKey9,
        binding.tvKey10,
        binding.tvKey11,
        binding.tvKey12,
        binding.tvKey13,
        binding.tvKey14,
        binding.tvKey15,
        binding.tvKey16,
        binding.tvKey17,
        binding.tvKey18,
        binding.tvKey19,
        binding.tvKey20,
        binding.tvKey21,
        binding.tvKey22,
        binding.tvKey24,
        binding.tvKey25,
        binding.tvKey26,
        binding.tvKey27,
        binding.tvKey29,
        binding.tvKey30,
        binding.tvKey33
    )
}