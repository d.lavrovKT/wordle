package com.lavrovdmitriii.wordle.en.keyadapter

import android.content.Context
import android.content.SharedPreferences
import android.view.View
import android.widget.TextView
import com.lavrovdmitriii.wordle.R
import com.lavrovdmitriii.wordle.constants.REQUIRED_SEND_WORD_COUNT
import com.lavrovdmitriii.wordle.constants.isVisibleModalHelp
import com.lavrovdmitriii.wordle.databinding.ActivityPlayEnBinding
import com.lavrovdmitriii.wordle.en.uielements.KeywordsElementsEn
import com.lavrovdmitriii.wordle.en.uielements.WordCharElementsEn
import com.lavrovdmitriii.wordle.keyadapter.KeyAdapter
import com.lavrovdmitriii.wordle.en.wordsbase.WordsEn

class KeyToucherEn(binding: ActivityPlayEnBinding, val myPreferences: SharedPreferences) {
    private val wordCharElements = WordCharElementsEn(binding)
    private val keywordsElements = KeywordsElementsEn(binding)
    private var currentWordGlobal = wordCharElements.wordCharElementsArr[0]
    private val thisBinding = binding
    private val myHandler = android.os.Handler()
    private var requiredCounter = 0
    private var delCounter = 0
    private val requiredFullWord = RequiredFullWordEn(binding)
    private val words = WordsEn.words
    private val keyAdapter = KeyAdapter()
    private val strBuilder = StringBuilder()

    fun requiredWord(context: Context, randomWord: String, points:String) {
        val fullWord = currentWordGlobal
        for (item in requiredCounter..5) {
            if (currentWordGlobal[4].text.isNotEmpty()) {
                val resultRequired = keyAdapter.getFullWord(fullWord, strBuilder)
                if (words.contains(resultRequired)) {
                    requiredFullWord.requiredWordInBase(
                        context,
                        fullWord,
                        item,
                        randomWord,
                        resultRequired,
                        thisBinding,
                        points
                    )
                    if (item != 5) {
                        currentWordGlobal = wordCharElements.wordCharElementsArr[item + 1]
                        for (index in 0..4) {
                            currentWordGlobal[index].setBackgroundResource(R.drawable.back_word_char_active)
                        }
                    }
                    delCounter++
                    requiredCounter++
                } else {
                    if (!isVisibleModalHelp) {
                        thisBinding.modalHelpSend.visibility = View.VISIBLE
                        isVisibleModalHelp = true
                        myPreferences.edit().putBoolean(REQUIRED_SEND_WORD_COUNT, isVisibleModalHelp).apply()
                        myHandler.postDelayed({
                            thisBinding.modalHelpSend.visibility = View.GONE
                        }, 10000)
                    }
                    thisBinding.tvFailRequired.visibility = View.VISIBLE
                    myHandler.postDelayed({
                        thisBinding.tvFailRequired.visibility = View.INVISIBLE
                    }, 1000)
                }

                break
            } else {
                thisBinding.tvFailEnter.visibility = View.VISIBLE
                myHandler.postDelayed({
                    thisBinding.tvFailEnter.visibility = View.INVISIBLE
                }, 1000)
            }
        }
    }

    fun addKeyChar(view: View) {
        val keywordElementsId = keywordsElements.getKeywordsElementsId(keywordsElements.keywordsArr)
        val keywordElementsText = keywordsElements.getKeywordsElementsText(keywordsElements.keywordsArr)
        val currentChar: TextView?
        for (item in 0..4) {
            if (currentWordGlobal[item].text.isEmpty()) {
                currentChar = currentWordGlobal[item]
            } else {
                continue
            }
            for (itemId in 0 until keywordElementsId.size) {
                if (view.id == keywordElementsId[itemId]) {
                    currentChar.text = keywordElementsText[itemId]
                    break
                } else {
                    continue
                }
            }
            currentWordGlobal[item].text = currentChar.text
            break
        }
        view.alpha = 0.7F
        myHandler.postDelayed({
            view.alpha = 1F
        }, 150)
    }

    fun deleteChar() {
        var currentWord: Array<TextView>
        for (word in 5 downTo delCounter) {
            currentWord = wordCharElements.wordCharElementsArr[word]
            if (currentWord[0].text.isNotEmpty()) {
                for (char in 4 downTo 0) {
                    if (currentWord[char].text.isNotEmpty()) {
                        currentWord[char].text = ""
                        break
                    }
                }
                break
            } else {
                continue
            }
        }
    }
}