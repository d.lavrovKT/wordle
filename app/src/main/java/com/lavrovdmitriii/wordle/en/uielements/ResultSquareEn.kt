package com.lavrovdmitriii.wordle.en.uielements

import com.lavrovdmitriii.wordle.databinding.ActivityPlayEnBinding

class ResultSquareEn(binding: ActivityPlayEnBinding) {
    var resultSquareArr = arrayOf(
        binding.tvResultChar1,
        binding.tvResultChar2,
        binding.tvResultChar3,
        binding.tvResultChar4,
        binding.tvResultChar5,
    )
}