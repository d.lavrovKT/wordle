package com.lavrovdmitriii.wordle.en.uielements

import com.lavrovdmitriii.wordle.databinding.ActivityPlayEnBinding

class WordCharElementsEn(binding: ActivityPlayEnBinding) {
    val word1 = arrayOf(
        binding.tvWchar11,
        binding.tvWchar12,
        binding.tvWchar13,
        binding.tvWchar14,
        binding.tvWchar15,
    )

    val word2 = arrayOf(
        binding.tvWchar21,
        binding.tvWchar22,
        binding.tvWchar23,
        binding.tvWchar24,
        binding.tvWchar25,
    )

    val word3 = arrayOf(
        binding.tvWchar31,
        binding.tvWchar32,
        binding.tvWchar33,
        binding.tvWchar34,
        binding.tvWchar35,
    )

    val word4 = arrayOf(
        binding.tvWchar41,
        binding.tvWchar42,
        binding.tvWchar43,
        binding.tvWchar44,
        binding.tvWchar45,
    )

    val word5 = arrayOf(
        binding.tvWchar51,
        binding.tvWchar52,
        binding.tvWchar53,
        binding.tvWchar54,
        binding.tvWchar55,
    )

    val word6 = arrayOf(
        binding.tvWchar61,
        binding.tvWchar62,
        binding.tvWchar63,
        binding.tvWchar64,
        binding.tvWchar65,
    )

    val wordCharElementsArr = arrayOf(word1, word2, word3, word4, word5, word6)
}