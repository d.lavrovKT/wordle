package com.lavrovdmitriii.wordle.keyadapter

import android.content.Context
import android.content.SharedPreferences
import android.widget.TextView
import com.lavrovdmitriii.wordle.R
import com.lavrovdmitriii.wordle.constants.counterLost
import com.lavrovdmitriii.wordle.constants.counterWin
import com.lavrovdmitriii.wordle.constants.winWords
import com.lavrovdmitriii.wordle.uielements.GuessedChar
import com.lavrovdmitriii.wordle.uielements.LocaleSelector
import com.lavrovdmitriii.wordle.ru.wordsbase.Words
import com.lavrovdmitriii.wordle.en.wordsbase.WordsEn
import kotlin.random.Random

class KeyAdapter {

    private val ruBase = Words.words
    private val enBase = WordsEn.words
    var currentFullWord: String? = null

    fun getRandomWord(): String {
        val currentBase: Array<String> = if (LocaleSelector.ruLangArr.contains(LocaleSelector.lang)) {
            ruBase
        } else {
            enBase
        }
        val randomValue = Random.nextInt(currentBase.size)
        return currentBase[randomValue]
    }

    fun getFullWord(fullWordArr: Array<TextView>, strBuilder: StringBuilder): String {
        for (item in 0..4) {
            currentFullWord = strBuilder.append(fullWordArr[item].text.toString()).toString()
        }
        val resultWord: String = currentFullWord.toString()
        currentFullWord = strBuilder.setLength(0).toString()
        return resultWord
    }

    fun replaceWinWordCounter(fullWordArrIndex: Int, myPreferences: SharedPreferences) {
        for (wordIndex in winWords.indices) {
            if (wordIndex == fullWordArrIndex) {
                winWords[wordIndex]++
                val winWordIndex = fullWordArrIndex + 1
                val keyWordIndex = "WINWORD$winWordIndex"
                myPreferences.edit().putInt(keyWordIndex, winWords[wordIndex]).apply()
            }
        }
    }

    fun trueRequired(fullWordArr: Array<TextView>, resultSquareArr: Array<TextView>, randomWord: String) {
        val greenColor = R.color.green
        for (item in fullWordArr.indices) {
            resultSquareArr[item].text = randomWord[item].toString()
            resultSquareArr[item].setBackgroundResource(greenColor)
            fullWordArr[item].setBackgroundResource(greenColor)
        }
        counterWin++
    }

    fun resultHelpArr(helpWordArr: MutableList<String>, randomWord: String): ArrayList<String> {
        val resultArr = arrayListOf<String>()
        for (item in GuessedChar.guessedChar.indices) {
            if (helpWordArr.contains(GuessedChar.guessedChar[item])) {
                for (index in helpWordArr.size - 1 downTo 0) {
                    if (helpWordArr[index] == GuessedChar.guessedChar[item]) {
                        helpWordArr.removeAt(index)
                    }
                }
            }
        }

        for (item in randomWord.indices) {
            if (helpWordArr.contains(randomWord[item].toString())) {
                resultArr.add(randomWord[item].toString())
            }
        }
        return resultArr
    }

    fun getHelpResult(resultArr: ArrayList<String>, context: Context, keywordsArr: Array<TextView>): String {
        var result = ""
        if (resultArr.size > 0) {
            val randomCharIndex = Random.nextInt(resultArr.size)
            val resultChar = resultArr[randomCharIndex]
            result = context.getString(R.string.help_text) + " " + "<" + resultChar + ">"

            for (item in keywordsArr.indices) {
                if (resultChar == keywordsArr[item].text.toString()) {
                    keywordsArr[item].setBackgroundResource(R.color.yellow)
                }
            }
        } else {
            result = context.getString(R.string.help_fail)
        }
        return result
    }

    fun writeResultWord(resultSquareArr: Array<TextView>, randomWord: String) {
        for (item in 0..4) {
            resultSquareArr[item].text = randomWord[item].toString()
        }
        counterLost++
    }
}