package com.lavrovdmitriii.wordle.ru.uielements

import com.lavrovdmitriii.wordle.databinding.ActivityPlayBinding

class ResultSquare (binding: ActivityPlayBinding) {
    var resultSquareArr = arrayOf(
        binding.tvResultChar1,
        binding.tvResultChar2,
        binding.tvResultChar3,
        binding.tvResultChar4,
        binding.tvResultChar5,
    )
}