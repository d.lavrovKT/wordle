package com.lavrovdmitriii.wordle.ru.keyadapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.view.View
import android.widget.TextView
import androidx.preference.PreferenceManager
import com.google.firebase.firestore.FirebaseFirestore
import com.lavrovdmitriii.wordle.R
import com.lavrovdmitriii.wordle.constants.*
import com.lavrovdmitriii.wordle.data.ReadDatabase
import com.lavrovdmitriii.wordle.data.User
import com.lavrovdmitriii.wordle.data.users
import com.lavrovdmitriii.wordle.databinding.ActivityPlayBinding
import com.lavrovdmitriii.wordle.keyadapter.KeyAdapter
import com.lavrovdmitriii.wordle.uielements.GuessedChar
import com.lavrovdmitriii.wordle.ru.uielements.KeywordsElements
import com.lavrovdmitriii.wordle.ru.uielements.ResultSquare
import com.lavrovdmitriii.wordle.statscounter.StatsCounter

class RequiredFullWord(binding: ActivityPlayBinding) {
    private var keywordsElements = KeywordsElements(binding)
    var greenArr = arrayListOf<String>()
    private val keyAdapter = KeyAdapter()
    private val keywordsArr = KeywordsElements(binding).keywordsArr
    private val db = FirebaseFirestore.getInstance()
    private var readDatabase = ReadDatabase()

    @SuppressLint("SetTextI18n")
    fun requiredWordInBase(
        context: Context,
        fullWordArr: Array<TextView>,
        fullWordArrIndex: Int,
        randomWord: String,
        playFullWord: String,
        binding: ActivityPlayBinding,
        points: String
    ): String {

        val resultArray = arrayListOf<String>()
        val resultSquareArr = ResultSquare(binding).resultSquareArr
        val greenColor = R.color.green
        val myPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val statsCounter = StatsCounter(myPreferences, db)

        if (playFullWord == randomWord) {
            keyAdapter.trueRequired(fullWordArr, resultSquareArr, randomWord)
            keyAdapter.replaceWinWordCounter(fullWordArrIndex, myPreferences)
            visibleResultGame(binding, counterWin, myPreferences, "WINSTATS", points, statsCounter)
        } else {
            for (item in fullWordArr.indices) {
                if (randomWord.contains(fullWordArr[item].text.toString())) {
                    GuessedChar.guessedChar.add(fullWordArr[item].text.toString())
                    for (index in 0..4) {
                        if (fullWordArr[item].text.toString() == randomWord[index].toString() && item == index) {
                            resultArray.add(fullWordArr[item].text.toString())
                            greenArr.add(fullWordArr[item].text.toString())
                            fullWordArr[item].setBackgroundResource(greenColor)
                            for (key in 0 until keywordsElements.keywordsArr.size) {
                                if (fullWordArr[item].text.toString() == keywordsElements.keywordsArr[key].text.toString()) {
                                    keywordsElements.keywordsArr[key].setBackgroundResource(greenColor)
                                }
                            }
                            break
                        } else {
                            resultArray.add(fullWordArr[item].text.toString())
                            GuessedChar.guessedChar.add(fullWordArr[item].text.toString())
                            fullWordArr[item].setBackgroundResource(R.color.yellow)
                            for (key in 0 until keywordsElements.keywordsArr.size) {
                                if (fullWordArr[item].text.toString() == keywordsElements.keywordsArr[key].text.toString()) {
                                    if (greenArr.contains(keywordsElements.keywordsArr[key].text.toString())) {
                                        keywordsElements.keywordsArr[key].setBackgroundResource(R.color.green)
                                    } else {
                                        keywordsElements.keywordsArr[key].setBackgroundResource(R.color.yellow)
                                    }
                                }
                            }
                        }
                    }
                } else {
                    GuessedChar.guessedChar.add(fullWordArr[item].text.toString())
                    fullWordArr[item].setBackgroundResource(R.color.red)
                    for (key in 0 until keywordsElements.keywordsArr.size) {
                        if (fullWordArr[item].text.toString() == keywordsElements.keywordsArr[key].text.toString()) {
                            keywordsElements.keywordsArr[key].setBackgroundResource(R.color.red)
                        }
                    }
                }
            }
            if (fullWordArrIndex == 5) {
                lostGameEvent(binding, resultSquareArr, randomWord, myPreferences, context, points, statsCounter)
            }
        }

        return resultArray.toString()
    }


    @SuppressLint("SetTextI18n")
    fun visibleResultGame(
        binding: ActivityPlayBinding,
        counter: Int,
        myPreferences: SharedPreferences,
        thisPreference: String,
        points: String,
        statsCounter: StatsCounter
    ) {
        statsCounter.updateUserPointsPref()
        statsCounter.updateUserPointsDB()
        binding.llResultGame.visibility = View.VISIBLE
        binding.keyWords.visibility = View.INVISIBLE
        binding.pointsNew.text = myPreferences.getInt(USER_POINTS, 0).toString() +
                "(+" +
                (myPreferences.getInt(USER_POINTS, 0) - points.toInt()).toString() +
                ")"
        myPreferences.edit().putInt(thisPreference, counter).apply()
        users = arrayListOf()
        readDatabase.setDatabaseCont()
        binding.winNum.text = myPreferences.getInt("WINSTATS", 0).toString()
        binding.loseNum.text = myPreferences.getInt("LOSTSTATS", 0).toString()
    }

    fun lostGameEvent(
        binding: ActivityPlayBinding,
        resultSquareArr: Array<TextView>,
        randomWord: String,
        myPreferences: SharedPreferences,
        context: Context,
        points: String,
        statsCounter: StatsCounter
    ) {
        binding.tvResultGameTitle.text = context.getString(R.string.lost_title)
        keyAdapter.writeResultWord(resultSquareArr, randomWord)
        visibleResultGame(binding, counterLost, myPreferences, "LOSTSTATS", points, statsCounter)
        binding.loseNum.text = myPreferences.getInt("LOSTSTATS", 0).toString()
        binding.helpBtn.visibility = View.INVISIBLE
    }

    fun helpVisible(randomWord: String, binding: ActivityPlayBinding, context: Context): String {
        val helpWordArr = KeywordsElements(binding).getKeywordsElementsText(keywordsElements.keywordsArr)
        val resultArr = keyAdapter.resultHelpArr(helpWordArr, randomWord)
        return keyAdapter.getHelpResult(resultArr, context, keywordsArr)
    }
}