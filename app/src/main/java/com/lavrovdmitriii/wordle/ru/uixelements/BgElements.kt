package com.lavrovdmitriii.wordle.ru.uixelements

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.lavrovdmitriii.wordle.R
import com.lavrovdmitriii.wordle.databinding.ActivityPlayBinding

class BgElements(var binding: ActivityPlayBinding, context: Context) {
    private val myPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    var elements = arrayOf(
        R.drawable.default_bg_image,
        R.drawable.bg_image_6,
        R.drawable.bg_image_7,
        R.drawable.bg_image_3,
        R.drawable.bg_image_4,
        R.drawable.bg_image_5,
        R.drawable.bg_image_9,
        R.drawable.bg_image_10,
        R.drawable.bg_image_15,
        R.drawable.bg_image_16,
        R.drawable.bg_image_17,
    )
    var index = 0

    fun changeBg() {
        if (index != elements.size - 1) {
            index++
        } else {
            index = 0
        }
        binding.bgBody.setBackgroundResource(elements[index])
        myPreferences.edit().putInt("bgIndex", index).apply()
    }
}