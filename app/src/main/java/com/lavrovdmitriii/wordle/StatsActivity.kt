package com.lavrovdmitriii.wordle

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.preference.PreferenceManager
import com.lavrovdmitriii.wordle.constants.BACK_IS_STATS
import com.lavrovdmitriii.wordle.constants.USER_POINTS
import com.lavrovdmitriii.wordle.databinding.ActivityStatsBinding

class StatsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStatsBinding
    private lateinit var myPreferences: SharedPreferences

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStatsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        myPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        val winResult = myPreferences.getInt("WINSTATS", 0)
        val lostResult = myPreferences.getInt("LOSTSTATS", 0)
        val percentResult = (winResult.toFloat() / (winResult.toFloat() + lostResult.toFloat()) * 100).toInt()

        with(binding) {
            AllGamesRes.text = (winResult + lostResult).toString()
            WinGamesRes.text = winResult.toString()
            LostGamesRes.text = lostResult.toString()
            PercentGamesRes.text = "$percentResult%"
            pointsRes.text = myPreferences.getInt(USER_POINTS, 0).toString()
            FirstWinRes.text = myPreferences.getInt("WINWORD1", 0).toString()
            SecondWinRes.text = myPreferences.getInt("WINWORD2", 0).toString()
            ThreeWinRes.text = myPreferences.getInt("WINWORD3", 0).toString()
            FourWinRes.text = myPreferences.getInt("WINWORD4", 0).toString()
            FiveWinRes.text = myPreferences.getInt("WINWORD5", 0).toString()
            SixWinRes.text = myPreferences.getInt("WINWORD6", 0).toString()
        }

        binding.backBtn.setOnClickListener {
            finish()
        }
        binding.globalStatsBtn.setOnClickListener {
            finish()
        }
    }
}