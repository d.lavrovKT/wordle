package com.lavrovdmitriii.wordle

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.google.firebase.firestore.FirebaseFirestore
import com.lavrovdmitriii.wordle.admob.Admob
import com.lavrovdmitriii.wordle.constants.USER_POINTS
import com.lavrovdmitriii.wordle.constants.helpCounter
import com.lavrovdmitriii.wordle.data.UserWords
import com.lavrovdmitriii.wordle.databinding.ActivityPlayEnBinding
import com.lavrovdmitriii.wordle.en.keyadapter.KeyToucherEn
import com.lavrovdmitriii.wordle.en.keyadapter.RequiredFullWordEn
import com.lavrovdmitriii.wordle.en.uielements.ResultSquareEn
import com.lavrovdmitriii.wordle.keyadapter.*
import com.lavrovdmitriii.wordle.uielements.GuessedChar
import com.lavrovdmitriii.wordle.en.uixelements.BgElementsEn
import com.lavrovdmitriii.wordle.statscounter.StatsCounter

class PlayEnActivity : AppCompatActivity() {
    private val myHandler = android.os.Handler()
    lateinit var keyToucher: KeyToucherEn
    private lateinit var binding: ActivityPlayEnBinding
    var admob = Admob(this)
    private lateinit var bgElements: BgElementsEn
    lateinit var randomWord: String
    private lateinit var myPreferences: SharedPreferences
    private lateinit var keyAdapter: KeyAdapter
    var throwFlag = false
    private val db = FirebaseFirestore.getInstance()
    private lateinit var statsCounter: StatsCounter
    lateinit var userWords: UserWords

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        admob.loadInterAd()
        keyAdapter = KeyAdapter()
        randomWord = keyAdapter.getRandomWord()
        GuessedChar.guessedChar = arrayListOf()
        helpCounter = 1

        binding = ActivityPlayEnBinding.inflate(layoutInflater)
        setContentView(binding.root)


        bgElements = BgElementsEn(binding, this)
        myPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        statsCounter = StatsCounter(myPreferences, db)
        bgElements.index = myPreferences.getInt("bgIndex", 0)
        binding.bgBody.setBackgroundResource(bgElements.elements[bgElements.index])
        keyToucher = KeyToucherEn(binding, myPreferences)
        if (helpCounter == 1) {
            binding.helpBtn.setImageResource(R.drawable.active_help)
        } else {
            binding.helpBtn.setImageResource(R.drawable.deactive_help)
        }
        userWords = UserWords(db, myPreferences)

        with(binding) {
            tvKeyEnter.setOnClickListener {
                keyToucher.requiredWord(this@PlayEnActivity, randomWord, myPreferences.getInt(USER_POINTS, 0).toString())
                opacityBtn(tvKeyEnter)
            }

            tvKeyDel.setOnClickListener {
                keyToucher.deleteChar()
                opacityBtn(tvKeyDel)
            }

            btnBackToMain.setOnClickListener {
                intent = Intent(this@PlayEnActivity, MainActivity::class.java)
                intent.putExtra("key", "backToMain")
                startActivity(intent)
                admob.showInterAd()
            }

            btnToRules.setOnClickListener {
                val intent = Intent(this@PlayEnActivity, RulesActivity::class.java)
                intent.putExtra("key", "playactivity")
                startActivity(intent)
            }

            imagesBtn.setOnClickListener {
                bgElements.changeBg()
            }

            agreeBtn.setOnClickListener {
                opacityBtn(agreeBtn)
                if (throwFlag) {
                    RequiredFullWordEn(this).lostGameEvent(
                        this,
                        ResultSquareEn(binding).resultSquareArr,
                        randomWord,
                        myPreferences,
                        this@PlayEnActivity,
                        myPreferences.getInt(USER_POINTS, 0).toString(),
                        statsCounter
                    )
                } else {
                    val resultHelp = RequiredFullWordEn(this).helpVisible(randomWord, this, this@PlayEnActivity)
                    helpCounter--
                    binding.helpBtn.setImageResource(R.drawable.deactive_help)
                    binding.tvHelpText.text = resultHelp
                    visibilityText(binding.tvHelpText)
                    myHandler.postDelayed({
                        admob.showInterAd()
                    }, 2000)
                    binding.helpBtn.visibility = View.VISIBLE
                    binding.keyWords.visibility = View.VISIBLE
                }
                modalWindow.visibility = View.GONE
            }
            sendWordImg.setOnClickListener {
                modalSendWord.visibility = View.VISIBLE
                sendBtn.setOnClickListener {
                    if (editWord.text.isEmpty()) {
                        failSendword.visibility = View.VISIBLE
                        failSendword.text = getString(R.string.validation_empty_input)
                        myHandler.postDelayed({
                            failSendword.visibility = View.INVISIBLE
                        }, 3000)
                    } else if (editWord.text.length != 5) {
                        failSendword.visibility = View.VISIBLE
                        failSendword.text = getString(R.string.validation_wrong_count_char)
                        myHandler.postDelayed({
                            failSendword.visibility = View.INVISIBLE
                        }, 3000)
                    } else {
                        userWords.addUserWordToDB(editWord.text.toString())
                        failSendword.visibility = View.VISIBLE
                        failSendword.text = getString(R.string.success_send_message)
                        myHandler.postDelayed({
                            failSendword.visibility = View.INVISIBLE
                            modalSendWord.visibility = View.GONE
                        }, 3000)
                    }
                }
            }
            closeModal.setOnClickListener {
                modalSendWord.visibility = View.GONE
            }

            helpSuccess.setOnClickListener {
                modalHelpSend.visibility = View.GONE
            }
        }
    }

    private fun visibilityText(textView: TextView) {
        textView.visibility = View.VISIBLE
        myHandler.postDelayed({
            textView.visibility = View.INVISIBLE
        }, 2000)
    }

    private fun opacityBtn(btn: View) {
        btn.alpha = 0.7F
        myHandler.postDelayed({
            btn.alpha = 1F
        }, 150)
    }

    fun goToStats(view: View) {
        val intent = Intent(this, GlobalStats::class.java)
        intent.putExtra("key", "playactivity")
        startActivity(intent)
    }

    fun throwGame(view: View) {
        throwFlag = true
        opacityBtn(binding.btnPass)
        binding.modalWindow.visibility = View.VISIBLE
        binding.keyWords.visibility = View.INVISIBLE
        binding.modalQuestion.text = getString(R.string.modal_pass_title)
        binding.helpBtn.visibility = View.INVISIBLE
    }

    fun helpBtnOnClick(view: View) {
        opacityBtn(binding.helpBtn)
        if (helpCounter == 1) {
            binding.modalWindow.visibility = View.VISIBLE
            binding.modalQuestion.text = getString(R.string.modal_help_title)
            binding.helpBtn.visibility = View.INVISIBLE
            binding.keyWords.visibility = View.INVISIBLE
            throwFlag = false
        } else {
            binding.tvHelpText.text = getString(R.string.you_already_help)
            visibilityText(binding.tvHelpText)
        }
    }

    fun disagreeOnClick(view: View) {
        opacityBtn(binding.disagreeBtn)
        binding.modalWindow.visibility = View.GONE
        binding.keyWords.visibility = View.VISIBLE
        binding.helpBtn.visibility = View.VISIBLE
        if (!throwFlag) {
            binding.keyWords.visibility = View.VISIBLE
        }
    }

    override fun onStart() {
        super.onStart()
        admob.loadInterAd()
    }

    override fun onResume() {
        super.onResume()
        admob.loadInterAd()
    }

    fun onClickRestart(view: View) {
        intent = Intent(this, PlayEnActivity::class.java)
        startActivity(intent)
        admob.showInterAd()
    }

    fun onClickWordKeys(view: View) {
        keyToucher.addKeyChar(view)
    }
}