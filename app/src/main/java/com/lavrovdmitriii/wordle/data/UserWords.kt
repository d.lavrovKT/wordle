package com.lavrovdmitriii.wordle.data

import android.content.SharedPreferences
import com.google.firebase.firestore.FirebaseFirestore
import com.lavrovdmitriii.wordle.constants.UNIQUE_USER_ID
import com.lavrovdmitriii.wordle.constants.userWord

class UserWords(private val db: FirebaseFirestore, val myPreferences: SharedPreferences) {
    private fun buildWord(word: String) {
        userWord["word"] = word
    }

    fun addUserWordToDB(word: String) {
        buildWord(word)
        db.collection("wordsUsers")
            .document(myPreferences.getString(UNIQUE_USER_ID, "default").toString())
            .set(userWord)
    }
}