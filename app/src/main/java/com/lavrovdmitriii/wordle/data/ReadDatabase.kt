package com.lavrovdmitriii.wordle.data


import android.annotation.SuppressLint
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query


class ReadDatabase() {
    private lateinit var db: FirebaseFirestore

    @SuppressLint("SetTextI18n")
    fun setDatabaseCont(): MutableList<User> {
        db = FirebaseFirestore.getInstance()
        db.collection("users")
            .orderBy("points", Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { it ->
                it.forEach {
                    val user = User()
                    user.nickname = it.get("nickname").toString()
                    user.uniqueID = it.get("uniqueID").toString()
                    user.points = it.get("points").toString().toInt()
                    user.flagID = it.get("flagID").toString()
                    users.add(user)
                }
            }
        return users
    }
}