package com.lavrovdmitriii.wordle.animationelements

import android.view.animation.Animation

class AnimationElements {
    var animationChar1: Animation? = null
    var animationChar2: Animation? = null
    var animationChar3: Animation? = null
    var animationChar4: Animation? = null
    var animationChar5: Animation? = null
    var animationChar6: Animation? = null
    var animationChar7: Animation? = null
    var animationChar8: Animation? = null
    val animationCharArr = arrayOf(
        animationChar1,
        animationChar2,
        animationChar3,
        animationChar4,
        animationChar5,
        animationChar6,
        animationChar7,
        animationChar8
    )
}