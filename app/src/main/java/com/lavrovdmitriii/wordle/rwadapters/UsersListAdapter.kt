package com.lavrovdmitriii.wordle.rwadapters

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lavrovdmitriii.wordle.GlobalStats
import com.lavrovdmitriii.wordle.R
import com.lavrovdmitriii.wordle.constants.UNIQUE_USER_ID
import com.lavrovdmitriii.wordle.data.User
import com.lavrovdmitriii.wordle.databinding.ActivityGlobalStatsBinding
import com.lavrovdmitriii.wordle.databinding.UserItemBinding

class UsersListAdapter(private val users: MutableList<User>, var myPreferences: SharedPreferences) :
    RecyclerView.Adapter<UsersListAdapter.UserHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false)
        return UserHolder(view)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: UserHolder, position: Int) {
        if (position == 0) {
            holder.profileIcon.setImageResource(R.drawable.kubok)
            holder.user.setBackgroundResource(R.drawable.background_with_shadow_gold)
        } else if (users[position].uniqueID == myPreferences.getString(UNIQUE_USER_ID, "-1").toString()) {
            holder.user.setBackgroundResource(R.color.green_user)
            holder.profileIcon.setImageResource(R.drawable.user_real_person)
        } else {
            holder.profileIcon.setImageResource(R.drawable.profile)
            holder.user.setBackgroundResource(R.drawable.background_with_shadow)
        }
        holder.position.text = (position + 1).toString()
        holder.nickname.text = users[position].nickname
        holder.points.text = users[position].points.toString()
        holder.userFlag.setCountryForNameCode(users[position].flagID)
    }

    class UserHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = UserItemBinding.bind(item)
        val user = binding.user
        val position = binding.position
        val nickname = binding.nickname
        val userFlag = binding.userFlag
        val points = binding.points
        val profileIcon = binding.profileImg
    }

}