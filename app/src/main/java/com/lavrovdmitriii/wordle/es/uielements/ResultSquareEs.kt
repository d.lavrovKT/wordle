package com.lavrovdmitriii.wordle.en.uielements

import com.lavrovdmitriii.wordle.databinding.ActivityPlayEsBinding

class ResultSquareEs(binding: ActivityPlayEsBinding) {
    var resultSquareArr = arrayOf(
        binding.tvResultChar1,
        binding.tvResultChar2,
        binding.tvResultChar3,
        binding.tvResultChar4,
        binding.tvResultChar5,
    )
}