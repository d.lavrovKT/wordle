package com.lavrovdmitriii.wordle.en.keyadapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.view.View
import android.widget.TextView
import androidx.preference.PreferenceManager
import com.google.firebase.firestore.FirebaseFirestore
import com.lavrovdmitriii.wordle.R
import com.lavrovdmitriii.wordle.constants.USER_POINTS
import com.lavrovdmitriii.wordle.constants.counterLost
import com.lavrovdmitriii.wordle.constants.counterWin
import com.lavrovdmitriii.wordle.data.ReadDatabase
import com.lavrovdmitriii.wordle.data.users
import com.lavrovdmitriii.wordle.databinding.ActivityPlayEsBinding
import com.lavrovdmitriii.wordle.en.uielements.KeywordsElementsEs
import com.lavrovdmitriii.wordle.en.uielements.ResultSquareEs
import com.lavrovdmitriii.wordle.keyadapter.KeyAdapter
import com.lavrovdmitriii.wordle.statscounter.StatsCounter
import com.lavrovdmitriii.wordle.uielements.*

class RequiredFullWordEs(binding: ActivityPlayEsBinding) {
    private val keywordsElements = KeywordsElementsEs(binding)
    var greenArr = arrayListOf<String>()
    private val keyAdapter = KeyAdapter()
    private val keywordsArr = KeywordsElementsEs(binding).keywordsArr
    private val db = FirebaseFirestore.getInstance()
    private var readDatabase = ReadDatabase()

    @SuppressLint("SetTextI18n")
    fun requiredWordInBase(
        context: Context,
        fullWordArr: Array<TextView>,
        fullWordArrIndex: Int,
        randomWord: String,
        playFullWord: String,
        binding: ActivityPlayEsBinding,
        points: String
    ): String {

        val resultArray = arrayListOf<String>()
        val resultSquareArr = ResultSquareEs(binding).resultSquareArr
        val greenColor = R.color.green
        val myPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val statsCounter = StatsCounter(myPreferences, db)

        if (playFullWord == randomWord) {
            keyAdapter.trueRequired(fullWordArr, resultSquareArr, randomWord)
            visibleResultGame(binding, counterWin, myPreferences, "WINSTATS", points, statsCounter)
            keyAdapter.replaceWinWordCounter(fullWordArrIndex, myPreferences)
        } else {
            for (item in fullWordArr.indices) {
                if (randomWord.contains(fullWordArr[item].text.toString())) {
                    GuessedChar.guessedChar.add(fullWordArr[item].text.toString())
                    for (index in 0..4) {
                        if (fullWordArr[item].text.toString() == randomWord[index].toString() && item == index) {
                            resultArray.add(fullWordArr[item].text.toString())
                            greenArr.add(fullWordArr[item].text.toString())
                            fullWordArr[item].setBackgroundResource(greenColor)
                            for (key in 0 until keywordsElements.keywordsArr.size) {
                                if (fullWordArr[item].text.toString() == keywordsElements.keywordsArr[key].text.toString()) {
                                    keywordsElements.keywordsArr[key].setBackgroundResource(greenColor)
                                }
                            }
                            break
                        } else {
                            resultArray.add(fullWordArr[item].text.toString())
                            GuessedChar.guessedChar.add(fullWordArr[item].text.toString())
                            fullWordArr[item].setBackgroundResource(R.color.yellow)
                            for (key in 0 until keywordsElements.keywordsArr.size) {
                                if (fullWordArr[item].text.toString() == keywordsElements.keywordsArr[key].text.toString()) {
                                    if (greenArr.contains(keywordsElements.keywordsArr[key].text.toString())) {
                                        keywordsElements.keywordsArr[key].setBackgroundResource(R.color.green)
                                    } else {
                                        keywordsElements.keywordsArr[key].setBackgroundResource(R.color.yellow)
                                    }
                                }
                            }
                        }
                    }
                } else {
                    GuessedChar.guessedChar.add(fullWordArr[item].text.toString())
                    fullWordArr[item].setBackgroundResource(R.color.red)
                    for (key in 0 until keywordsElements.keywordsArr.size) {
                        if (fullWordArr[item].text.toString() == keywordsElements.keywordsArr[key].text.toString()) {
                            keywordsElements.keywordsArr[key].setBackgroundResource(R.color.red)
                        }
                    }
                }
            }
            if (fullWordArrIndex == 5) {
                lostGameEvent(binding, resultSquareArr, randomWord, myPreferences, context, points, statsCounter)
            }
        }

        return resultArray.toString()
    }


    @SuppressLint("SetTextI18n")
    fun visibleResultGame(
        binding: ActivityPlayEsBinding,
        counter: Int,
        myPreferences: SharedPreferences,
        thisPreference: String,
        points: String,
        statsCounter: StatsCounter
    ) {
        statsCounter.updateUserPointsPref()
        statsCounter.updateUserPointsDB()
        binding.llResultGame.visibility = View.VISIBLE
        binding.keyWords.visibility = View.INVISIBLE
        binding.pointsNew.text = myPreferences.getInt(USER_POINTS, 0).toString() +
                "(+" +
                (myPreferences.getInt(USER_POINTS, 0) - points.toInt()).toString() +
                ")"
        myPreferences.edit().putInt(thisPreference, counter).apply()
        users = arrayListOf()
        readDatabase.setDatabaseCont()
        binding.winNum.text = myPreferences.getInt("WINSTATS", 0).toString()
        binding.loseNum.text = myPreferences.getInt("LOSTSTATS", 0).toString()
    }

    fun lostGameEvent(
        binding: ActivityPlayEsBinding,
        resultSquareArr: Array<TextView>,
        randomWord: String,
        myPreferences: SharedPreferences,
        context: Context,
        points: String,
        statsCounter: StatsCounter
    ) {
        binding.tvResultGameTitle.text = context.getString(R.string.lost_title)
        keyAdapter.writeResultWord(resultSquareArr, randomWord)
        visibleResultGame(binding, counterLost, myPreferences, "LOSTSTATS", points, statsCounter)
        binding.loseNum.text = myPreferences.getInt("LOSTSTATS", 0).toString()
        binding.helpBtn.visibility = View.INVISIBLE
    }

    fun helpVisible(randomWord: String, binding: ActivityPlayEsBinding, context: Context): String {
        val helpWordArr = KeywordsElementsEs(binding).getKeywordsElementsText(keywordsElements.keywordsArr)
        val resultArr = keyAdapter.resultHelpArr(helpWordArr, randomWord)
        return keyAdapter.getHelpResult(resultArr, context, keywordsArr)
    }
}