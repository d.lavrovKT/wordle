package com.lavrovdmitriii.wordle

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.preference.PreferenceManager
import com.google.firebase.firestore.FirebaseFirestore
import com.lavrovdmitriii.wordle.constants.*
import com.lavrovdmitriii.wordle.data.ReadDatabase
import com.lavrovdmitriii.wordle.data.users
import com.lavrovdmitriii.wordle.databinding.ActivityMainBinding
import com.lavrovdmitriii.wordle.regform.RegFormAdapter
import com.lavrovdmitriii.wordle.statscounter.StatsCounter
import com.lavrovdmitriii.wordle.uielements.MainScreenElements
import com.lavrovdmitriii.wordle.uielements.LocaleSelector


class MainActivity : AppCompatActivity() {
    private val myHandler = android.os.Handler()
    private lateinit var binding: ActivityMainBinding
    private lateinit var screenElements: Array<View>
    private lateinit var myPreferences: SharedPreferences
    private lateinit var regFormAdapter: RegFormAdapter
    private lateinit var db: FirebaseFirestore
    private var readDatabase = ReadDatabase()
    private lateinit var statsCounter: StatsCounter

    @SuppressLint("CommitPrefEdits", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        db = FirebaseFirestore.getInstance()
        myPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        regFormAdapter = RegFormAdapter(binding, myPreferences, db, this)
        statsCounter = StatsCounter(myPreferences, db)
        statsCounter.updateUserPointsPref()
        statsCounter.updateUserPointsDB()
        isRegUser = myPreferences.getBoolean(IS_REG_USER, false)
        if (!isRegUser) {
            regFormAdapter.regFormShow(myHandler, this)
            users = arrayListOf()
            readDatabase.setDatabaseCont()
        } else {
            if (intent.getStringExtra("key") != BACK_IS_STATS) {
                users = arrayListOf()
                readDatabase.setDatabaseCont()
            }
        }

        isVisibleModalHelp = myPreferences.getBoolean(REQUIRED_SEND_WORD_COUNT, false)

        screenElements = MainScreenElements(binding).screenElements
        counterWin = myPreferences.getInt("WINSTATS", 0)
        counterLost = myPreferences.getInt("LOSTSTATS", 0)
        val percentResult = (counterWin.toFloat() / (counterWin.toFloat() + counterLost.toFloat()) * 100).toInt()
        for (item in winWords.indices) {
            val keyCounter = item + 1
            winWords[item] = myPreferences.getInt("WINWORD$keyCounter", 0)
        }

        uniqueUserId = myPreferences.getString(UNIQUE_USER_ID, "-1").toString()
        binding.nicknameHeader.text = myPreferences.getString(USER_NAME, "Nickname")
        binding.currentFlag.setCountryForNameCode(myPreferences.getString(COUNTRY_CODE_USER, "US"))

        binding.WinNumber.text = counterWin.toString()
        binding.LostNumber.text = myPreferences.getInt(USER_POINTS, 0).toString()
        binding.GameNumber.text = (counterWin + counterLost).toString()
        binding.PercentNumber.text = percentResult.toString()


        with(binding) {
            btnStart.setOnClickListener {
                val intent: Intent = if (LocaleSelector.ruLangArr.contains(LocaleSelector.lang)) {
                    Intent(this@MainActivity, PlayActivity::class.java)
                } else {
                    when (LocaleSelector.lang) {
                        "en" -> Intent(this@MainActivity, PlayEnActivity::class.java)
                        "es" -> Intent(this@MainActivity, PlayEsActivity::class.java)
                        else -> Intent(this@MainActivity, PlayActivity::class.java)
                    }
                }
                startActivity(intent)
            }
            btnRules.setOnClickListener {
                val intent = Intent(this@MainActivity, RulesActivity::class.java)
                startActivity(intent)
            }
            btnExitGame.setOnClickListener {
                finishAffinity()
            }

            btnStats.setOnClickListener {
                startActivity(Intent(this@MainActivity, GlobalStats::class.java))
            }
            btnSettings.setOnClickListener {
                startActivity(Intent(this@MainActivity, SettingsActivity::class.java))
            }

            btnShare.setOnClickListener {
                val sendIntent = Intent(Intent.ACTION_SEND)
                sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text) + " - https://play.google.com/store/apps/details?id=com.lavrovdmitriii.wordle")
                sendIntent.type = "text/plain"
                startActivity(Intent.createChooser(sendIntent, getString(R.string.share_btn)))
            }
        }
    }
}