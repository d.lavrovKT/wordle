package com.lavrovdmitriii.wordle

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.preference.PreferenceManager
import com.lavrovdmitriii.wordle.databinding.ActivitySettingsBinding
import com.lavrovdmitriii.wordle.uielements.LocaleSelector

class SettingsActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySettingsBinding
    private lateinit var myPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        myPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        if (LocaleSelector.ruLangArr.contains(LocaleSelector.lang)) {
            binding.IconRu.setImageResource(R.drawable.ru_active)
        } else {
            when(LocaleSelector.lang) {
                "en" ->  binding.IconEn.setImageResource(R.drawable.gbr_active)
                "es" ->  binding.IconEs.setImageResource(R.drawable.esp_active)
            }
        }

        binding.IconRu.setOnClickListener {
            binding.IconRu.setImageResource(R.drawable.ru_active)
            LocaleSelector.lang = "ru"
            binding.IconEn.setImageResource(R.drawable.gbr_deactive)
            binding.IconEs.setImageResource(R.drawable.esp_deactive)
            myPreferences.edit().putString("currentBase", LocaleSelector.lang).apply()
        }

        binding.IconEn.setOnClickListener {
            binding.IconEn.setImageResource(R.drawable.gbr_active)
            LocaleSelector.lang = "en"
            binding.IconRu.setImageResource(R.drawable.ru_deactive)
            binding.IconEs.setImageResource(R.drawable.esp_deactive)
            myPreferences.edit().putString("currentBase", LocaleSelector.lang).apply()
        }

        binding.IconEs.setOnClickListener {
            binding.IconEs.setImageResource(R.drawable.esp_active)
            LocaleSelector.lang = "es"
            binding.IconRu.setImageResource(R.drawable.ru_deactive)
            binding.IconEn.setImageResource(R.drawable.gbr_deactive)
            myPreferences.edit().putString("currentBase", LocaleSelector.lang).apply()
        }

        binding.backBtn.setOnClickListener {
            finish()
        }

    }
}