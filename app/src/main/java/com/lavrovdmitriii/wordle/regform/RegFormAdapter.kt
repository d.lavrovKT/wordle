package com.lavrovdmitriii.wordle.regform

import android.content.Context
import android.content.SharedPreferences
import android.os.Handler
import android.view.View
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import com.lavrovdmitriii.wordle.R
import com.lavrovdmitriii.wordle.constants.*
import com.lavrovdmitriii.wordle.data.ReadDatabase
import com.lavrovdmitriii.wordle.data.users
import com.lavrovdmitriii.wordle.databinding.ActivityMainBinding
import java.util.*
import kotlin.random.Random

class RegFormAdapter(val binding: ActivityMainBinding, val myPreferences: SharedPreferences, private val db: FirebaseFirestore, var context: Context) {

    private lateinit var random: Random
    private val readDatabase = ReadDatabase()

    fun regFormShow(myHandler: Handler, context: Context) {
        modalRegShowEl()
        binding.regConfirm.setOnClickListener {
            if (binding.nickname.text.isNotEmpty()) {
                if (binding.nickname.text.length < 15) {
                    isRegUser = true
                    userName = binding.nickname.text.toString()
                    countryCode = binding.countryCodePicker.selectedCountryNameCode
                    defaultLocale = Locale.getDefault().language
                    uniqueUserId = getUniqueUserId(countryCode, defaultLocale)
                    getMyPrefReg()
                    binding.nicknameHeader.text = userName
                    binding.currentFlag.setCountryForNameCode(countryCode)
                    modalRegHideEl()
                    buildUser(userName, countryCode, userPoints)
                    addUserToDB()
                    users = arrayListOf()
                    readDatabase.setDatabaseCont()
                } else {
                    binding.failRegister.visibility = View.VISIBLE
                    binding.failRegister.text = context.getString(R.string.validation_long_nick)
                    myHandler.postDelayed({
                        binding.failRegister.visibility = View.INVISIBLE
                    }, 3000)
                }
            } else {
                binding.failRegister.visibility = View.VISIBLE
                myHandler.postDelayed({
                    binding.failRegister.visibility = View.INVISIBLE
                }, 3000)
            }
        }
    }

    private fun getMyPrefReg() {
        myPreferences.edit().putBoolean(IS_REG_USER, isRegUser).apply()
        myPreferences.edit().putString(USER_NAME, userName).apply()
        myPreferences.edit().putString(COUNTRY_CODE_USER, countryCode).apply()
        myPreferences.edit().putString(UNIQUE_USER_ID, uniqueUserId).apply()
    }

    private fun modalRegShowEl() {
        binding.modalRegForm.visibility = View.VISIBLE
        binding.btnStart.visibility = View.INVISIBLE
        binding.btnRules.visibility = View.INVISIBLE
        binding.profileLayout.visibility = View.INVISIBLE
    }

    private fun modalRegHideEl() {
        binding.modalRegForm.visibility = View.INVISIBLE
        binding.btnStart.visibility = View.VISIBLE
        binding.btnRules.visibility = View.VISIBLE
        binding.profileLayout.visibility = View.VISIBLE
    }

    private fun buildUser(nickname: String, countryCode: String, points: Int) {
        user["nickname"] = nickname
        user["flagID"] = countryCode
        user["points"] = points
        user["uniqueID"] = uniqueUserId
        user["defaultLocale"] = Locale.getDefault().language
    }

    private fun addUserToDB() {
        db.collection("users")
            .document(uniqueUserId)
            .set(user)
    }

    private fun getUniqueUserId(flagId: String, defaultLocale: String): String {
        var result = ""
        random = Random
        result = defaultLocale + "–" + flagId + (random.nextInt(1000000000 - 0)).toString()
        return result
    }
}