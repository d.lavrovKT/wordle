package com.lavrovdmitriii.wordle.admob

import android.content.Context
import com.yandex.mobile.ads.common.AdRequest
import com.yandex.mobile.ads.interstitial.InterstitialAd

class YandexMob(val context: Context) {
    private lateinit var yandexAd: InterstitialAd

    fun loadMob() {
        yandexAd = InterstitialAd(context)
        yandexAd.setAdUnitId("R-M-1587381-1")
        val adRequest = AdRequest.Builder().build()
        yandexAd.loadAd(adRequest)
    }

    fun showMob() {
        if (yandexAd.isLoaded) {
            yandexAd.show()
            loadMob()
        }
    }
}