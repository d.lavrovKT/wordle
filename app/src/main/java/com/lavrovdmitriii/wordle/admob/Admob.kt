package com.lavrovdmitriii.wordle.admob

import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback

class Admob (val context: AppCompatActivity) {

    private var interAd: InterstitialAd? = null
    var yandexMob = YandexMob(context)

    fun loadInterAd() {
        val adRequest = AdRequest.Builder().build()
        yandexMob.loadMob()
        InterstitialAd.load(context,
            "ca-app-pub-1173862103025440/7674924390",
            adRequest, object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(p0: LoadAdError) {
                    interAd = null
                }

                override fun onAdLoaded(p0: InterstitialAd) {
                    interAd = p0
                }
            })
    }

    fun showInterAd() {
        if (interAd != null) {
            interAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
                override fun onAdDismissedFullScreenContent() {
                    interAd = null
                    loadInterAd()
                }

                override fun onAdFailedToShowFullScreenContent(p0: AdError) {
                    interAd = null
                    loadInterAd()
                }

                override fun onAdShowedFullScreenContent() {
                    interAd = null
                    loadInterAd()
                }

                override fun onAdImpression() {
                    interAd = null
                    loadInterAd()
                }
            }
            interAd?.show(context)
        } else {
            yandexMob.showMob()
        }
    }
}