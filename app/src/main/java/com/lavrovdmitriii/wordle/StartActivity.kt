package com.lavrovdmitriii.wordle

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.preference.PreferenceManager
import com.lavrovdmitriii.wordle.animationelements.AnimationElements
import com.lavrovdmitriii.wordle.databinding.ActivityStartBinding
import com.lavrovdmitriii.wordle.uielements.LocaleSelector
import com.lavrovdmitriii.wordle.uielements.LogoChars
import java.util.*

class StartActivity : AppCompatActivity() {
    private lateinit var logoChars: Array<TextView>
    private val myHandler = android.os.Handler()
    private lateinit var binding: ActivityStartBinding
    private lateinit var myPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        myPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        if (myPreferences.getString("currentBase", "default") == "default") {
            LocaleSelector.lang = Locale.getDefault().language
        } else {
            LocaleSelector.lang = myPreferences.getString("currentBase", "default").toString()
        }

        logoChars = LogoChars(binding).logoChars
        val animationElements = AnimationElements().animationCharArr
        for (item in animationElements.indices) {
            animationElements[item] = AnimationUtils.loadAnimation(this, R.anim.text_animation)
        }

        var delay: Long = 0
        for (item in animationElements.indices) {
            animationElements[item]?.let { animationDelay(it, logoChars[item], delay) }
            delay += 300
        }

        myHandler.postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
        }, 3000)
    }

    private fun animationDelay(animationChar: Animation, tvLogoChar: TextView, delay: Long) {
        myHandler.postDelayed({
            animationLogo(tvLogoChar, animationChar)
            tvLogoChar.alpha = 1F
        }, delay)
    }

    private fun animationLogo(textView: TextView, animation: Animation) {
        textView.startAnimation(animation)
    }

}